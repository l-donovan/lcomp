# LComp
### Generic MIPS CPU emulator
[![pipeline status](https://gitlab.com/l-donovan/lcomp/badges/master/pipeline.svg)](https://gitlab.com/l-donovan/lcomp/commits/master)

See [lcomp-example](https://gitlab.com/l-donovan/lcomp-example) for an example