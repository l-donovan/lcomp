package luke.lcomp.base;

public interface Instruction {
    void parse(String instructionString);
    default void run(LComp lcomp) {}

    default String getName() {
        return this.getClass().getSimpleName();
    }
}
