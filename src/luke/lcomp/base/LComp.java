package luke.lcomp.base;

import java.util.ArrayList;

public class LComp {
    private ArrayList<Instruction> instructions;
    private int[] registers = new int[34];
    private byte[] memory = new byte[512];
    private int PC = 0;
    private int nPC = 4;

    public LComp() {
        for (int i = 0; i < this.registers.length; i++) {
            this.registers[i] = 0;
        }
        for (int i = 0; i < this.memory.length; i++) {
            this.memory[i] = 0;
        }
    }

    public void loadInstructions(ArrayList<Instruction> instructions) {
        this.instructions = instructions;
    }

    public void run() {
        while (this.PC / 4 < this.instructions.size()) {
            Instruction inst = this.instructions.get(this.PC / 4);
            System.out.println(inst);
            inst.run(this);
        }
    }

    public int getRegister(int index) {
        return this.registers[index];
    }

    public void setRegister(int index, int value) {
        this.registers[index] = value;
    }

    public int getPC() {
        return this.PC;
    }

    public void setPC(int value) {
        this.PC = value;
    }

    public void advancePC(int value) {
        this.PC = this.nPC;
        this.nPC += value;
    }

    public int getnPC() {
        return this.nPC;
    }

    public void setnPC(int value) {
        this.nPC = value;
    }

    public void advancenPC(int value) {
        this.nPC += value;
    }

    public byte byteAt(int addr) {
        return this.memory[addr];
    }

    public String toString() {
        return "LComp instance";
    }
}
