package luke.lcomp.base;

import luke.lcomp.instructions.*;

import java.util.HashMap;
import java.util.Map;

public enum InstructionMap {
    NOOP   ("00000000000000000000000000000000", NOOP.class),
    ADD    ("000000XXXXXXXXXXXXXXX00000100000", ADD.class),
    ADDI   ("001000XXXXXXXXXXXXXXXXXXXXXXXXXX", ADDI.class),
    ADDIU  ("001001XXXXXXXXXXXXXXXXXXXXXXXXXX", ADDIU.class),
    ADDU   ("000000XXXXXXXXXXXXXXX00000100001", ADDU.class),
    AND    ("000000XXXXXXXXXXXXXXX00000100100", AND.class),
    ANDI   ("001100XXXXXXXXXXXXXXXXXXXXXXXXXX", ANDI.class),
    BEQ    ("000100XXXXXXXXXXXXXXXXXXXXXXXXXX", BEQ.class),
    BGEZ   ("000001XXXXX00001XXXXXXXXXXXXXXXX", BGEZ.class),
    BGEZAL ("000001XXXXX10001XXXXXXXXXXXXXXXX", BGEZAL.class),
    BGTZ   ("000111XXXXX00000XXXXXXXXXXXXXXXX", BGTZ.class),
    BLEZ   ("000110XXXXX00000XXXXXXXXXXXXXXXX", BLEZ.class),
    BLTZ   ("000001XXXXX00000XXXXXXXXXXXXXXXX", BLTZ.class),
    BLTZAL ("000001XXXXX10000XXXXXXXXXXXXXXXX", BLTZAL.class),
    BNE    ("000101XXXXXXXXXXXXXXXXXXXXXXXXXX", BNE.class),
    DIV    ("000000XXXXXXXXXX0000000000011010", DIV.class),
    DIVU   ("000000XXXXXXXXXX0000000000011011", DIVU.class),
    J      ("000010XXXXXXXXXXXXXXXXXXXXXXXXXX", J.class),
    JAL    ("000011XXXXXXXXXXXXXXXXXXXXXXXXXX", JAL.class),
    JR     ("000000XXXXX000000000000000001000", JR.class),
    LB     ("100000XXXXXXXXXXXXXXXXXXXXXXXXXX", LB.class),
    LUI    ("001111XXXXXXXXXXXXXXXXXXXXXXXXXX", LUI.class),
    LW     ("100011XXXXXXXXXXXXXXXXXXXXXXXXXX", LW.class),
    MFHI   ("0000000000000000XXXXX00000010000", MFHI.class),
    MFLO   ("0000000000000000XXXXX00000010010", MFLO.class),
    MULT   ("000000XXXXXXXXXX0000000000011000", MULT.class),
    MULTU  ("000000XXXXXXXXXX0000000000011001", MULTU.class),
    OR     ("000000XXXXXXXXXXXXXXX00000100101", OR.class),
    ORI    ("001101XXXXXXXXXXXXXXXXXXXXXXXXXX", ORI.class);

    private static final Map<String, Class> map = new HashMap<>();

    static {
        for (InstructionMap m : InstructionMap.values()) {
            map.put(m.getInstructionPattern(), m.getInstructionClass());
        }
    }

    private String instructionPattern;
    private Class instructionClass;

    InstructionMap(String instructionPattern, Class instructionClass) {
        this.instructionPattern = instructionPattern;
        this.instructionClass = instructionClass;
    }

    public String getInstructionPattern() { return this.instructionPattern; }
    public Class  getInstructionClass()   { return this.instructionClass; }

    public static Class getClassByPattern(String pattern) {
        boolean match;
        for (String mapPattern : map.keySet()) {
            match = true;
            for (int i = 0; i < pattern.length(); i++) {
                if (mapPattern.charAt(i) != pattern.charAt(i) && mapPattern.charAt(i) != 'X') {
                    match = false;
                    break;
                }
            }
            if (match) {
                return map.get(mapPattern);
            }
        }
        return NOOP.class;
    }

    public static Instruction getInstructionByPattern(String pattern) {
        Class c = getClassByPattern(pattern);
        Instruction inst = null;

        try {
            inst = (Instruction) c.newInstance();
            inst.parse(pattern);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        return inst;
    }
}
