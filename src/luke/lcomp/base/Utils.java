package luke.lcomp.base;

import java.util.ArrayList;
import java.util.Map;

public class Utils {
    public static ArrayList<Instruction> parseInstructions(String[] instructions) {
        ArrayList<Instruction> outputInstructions = new ArrayList<>();

        for (String instruction : instructions) {
            Instruction inst = InstructionMap.getInstructionByPattern(instruction);
            outputInstructions.add(inst);
            System.out.println(inst);
        }

        return outputInstructions;
    }

    public static void parseOperand(String instructionString, Map<String, Integer> operands, String key, int start, int stop) {
        operands.put(key, Integer.parseInt(instructionString.substring(start, stop), 2));
    }

    public static void parseOperand(String instructionString, Map<String, Integer> operands, String key, int start, int stop, boolean signed) {
        int i = Integer.parseInt(instructionString.substring(start, stop), 2);

        if (signed) {
            operands.put(key, (int)((short)i));
        } else {
            operands.put(key, i);
        }
    }

    public static int signed(int val) {
        return (int)((short)val);
    }
}
