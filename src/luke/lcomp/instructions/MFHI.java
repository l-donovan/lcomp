package luke.lcomp.instructions;

import luke.lcomp.base.Instruction;
import luke.lcomp.base.LComp;
import luke.lcomp.base.Utils;

import java.util.HashMap;
import java.util.Map;

import static luke.lcomp.base.Constants.HI;

public class MFHI implements Instruction {
    private Map<String, Integer> operands;

    public void parse(String instructionString) {
        this.operands = new HashMap<>();

        Utils.parseOperand(instructionString, this.operands, "d", 16, 21);
    }

    private int get(String key) {
        return this.operands.get(key);
    }

    public void run(LComp lcomp) {
        lcomp.setRegister(this.get("d"), lcomp.getRegister(HI));

        lcomp.advancePC(4);
    }

    public String toString() {
        return "MFHI $" + this.get("d");
    }
}
