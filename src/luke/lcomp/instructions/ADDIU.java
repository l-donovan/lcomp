package luke.lcomp.instructions;

import luke.lcomp.base.Instruction;
import luke.lcomp.base.LComp;
import luke.lcomp.base.Utils;

import java.util.HashMap;
import java.util.Map;

public class ADDIU implements Instruction {
    private Map<String, Integer> operands;

    public void parse(String instructionString) {
        this.operands = new HashMap<>();

        Utils.parseOperand(instructionString, this.operands, "s", 6, 11);
        Utils.parseOperand(instructionString, this.operands, "t", 11, 16);
        Utils.parseOperand(instructionString, this.operands, "i", 16, 32);
    }

    private int get(String key) {
        return this.operands.get(key);
    }

    public void run(LComp lcomp) {
        lcomp.setRegister(this.get("t"), lcomp.getRegister(this.get("s")) + this.get("i"));
        lcomp.advancePC(4);
    }

    public String toString() {
        return "ADDIU $" + this.get("t") + ", $" + this.get("s") + ", " + this.get("i");
    }
}
