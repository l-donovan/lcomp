package luke.lcomp.instructions;

import luke.lcomp.base.Instruction;
import luke.lcomp.base.LComp;
import luke.lcomp.base.Utils;

import java.util.HashMap;
import java.util.Map;

public class BNE implements Instruction {
    private Map<String, Integer> operands;

    public void parse(String instructionString) {
        this.operands = new HashMap<>();

        Utils.parseOperand(instructionString, this.operands, "s", 6, 11);
        Utils.parseOperand(instructionString, this.operands, "t", 11, 16);
        Utils.parseOperand(instructionString, this.operands, "i", 16, 32);
    }

    private int get(String key) {
        return this.operands.get(key);
    }

    public void run(LComp lcomp) {
        if (lcomp.getRegister(this.get("s")) != lcomp.getRegister((this.get("t")))) {
            lcomp.setPC(lcomp.getnPC());
            lcomp.setnPC((lcomp.getPC() & 0xF0000000) | (this.get("i") << 2));
        }

        lcomp.advancePC(4);
    }

    public String toString() {
        return "BNE $" + this.get("s") + ", $" + this.get("t") + ", " + this.get("i");
    }
}
