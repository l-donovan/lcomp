package luke.lcomp.instructions;

import luke.lcomp.base.Instruction;
import luke.lcomp.base.LComp;
import luke.lcomp.base.Utils;

import java.util.HashMap;
import java.util.Map;

public class LW implements Instruction {
    private Map<String, Integer> operands;

    public void parse(String instructionString) {
        this.operands = new HashMap<>();

        Utils.parseOperand(instructionString, this.operands, "s", 6, 11);
        Utils.parseOperand(instructionString, this.operands, "t", 11, 16);
        Utils.parseOperand(instructionString, this.operands, "i", 16, 32);
    }

    private int get(String key) {
        return this.operands.get(key);
    }

    public void run(LComp lcomp) {
        int addr = this.get("s") + this.get("i");
        int word = lcomp.byteAt(addr) << 24 | lcomp.byteAt(addr + 1) << 16 | lcomp.byteAt(addr + 2) << 8 | lcomp.byteAt(addr + 3);
        lcomp.setRegister(this.get("t"), word);

        lcomp.advancePC(4);
    }

    public String toString() {
        return "LW $" + this.get("t") + ", " + this.get("i") + "(" + this.get("s") + ")";
    }
}
