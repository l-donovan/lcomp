package luke.lcomp.instructions;

import luke.lcomp.base.Instruction;
import luke.lcomp.base.LComp;
import luke.lcomp.base.Utils;

import java.util.HashMap;
import java.util.Map;

public class JAL implements Instruction {
    private Map<String, Integer> operands;

    public void parse(String instructionString) {
        this.operands = new HashMap<>();

        Utils.parseOperand(instructionString, this.operands, "i", 6, 32);
    }

    private int get(String key) {
        return this.operands.get(key);
    }

    public void run(LComp lcomp) {
        lcomp.setRegister(31, lcomp.getPC() + 8);
        lcomp.setPC(lcomp.getnPC());
        lcomp.setnPC((lcomp.getPC() & 0xF0000000) | (this.get("i") << 2));

        lcomp.advancePC(4);
    }

    public String toString() {
        return "JAL " + this.get("i");
    }
}
