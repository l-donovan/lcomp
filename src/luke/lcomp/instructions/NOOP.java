package luke.lcomp.instructions;

import luke.lcomp.base.Instruction;
import luke.lcomp.base.LComp;

import java.util.Map;

public class NOOP implements Instruction {
    // NOTE: The NOOP instruction is technically SLL $0, $0, 0

    private Map<String, Integer> operands;

    public void parse(String instructionString) { }

    public void run(LComp lcomp) {
        lcomp.advancePC(4);
    }

    public String toString() {
        return "NOOP";
    }
}
