package luke.lcomp.instructions;

import luke.lcomp.base.Instruction;
import luke.lcomp.base.LComp;
import luke.lcomp.base.Utils;

import java.util.HashMap;
import java.util.Map;

public class LB implements Instruction {
    private Map<String, Integer> operands;

    public void parse(String instructionString) {
        this.operands = new HashMap<>();

        Utils.parseOperand(instructionString, this.operands, "s", 6, 11);
        Utils.parseOperand(instructionString, this.operands, "t", 11, 16);
        Utils.parseOperand(instructionString, this.operands, "i", 16, 32);
    }

    private int get(String key) {
        return this.operands.get(key);
    }

    public void run(LComp lcomp) {
        lcomp.setRegister(this.get("t"), lcomp.byteAt(this.get("s") + this.get("i")));

        lcomp.advancePC(4);
    }

    public String toString() {
        return "LB $" + this.get("t") + ", " + this.get("i") + "(" + this.get("s") + ")";
    }
}
