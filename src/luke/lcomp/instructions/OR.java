package luke.lcomp.instructions;

import luke.lcomp.base.Instruction;
import luke.lcomp.base.LComp;
import luke.lcomp.base.Utils;

import java.util.HashMap;
import java.util.Map;

public class OR implements Instruction {
    private Map<String, Integer> operands;

    public void parse(String instructionString) {
        this.operands = new HashMap<>();

        Utils.parseOperand(instructionString, this.operands, "s", 6, 11);
        Utils.parseOperand(instructionString, this.operands, "t", 11, 16);
        Utils.parseOperand(instructionString, this.operands, "d", 16, 21);
    }

    private int get(String key) {
        return this.operands.get(key);
    }

    public void run(LComp lcomp) {
        lcomp.setRegister(this.get("d"), lcomp.getRegister(this.get("s")) | lcomp.getRegister(this.get("t")));

        lcomp.advancePC(4);
    }

    public String toString() {
        return "OR $" + this.get("d") + ", $" + this.get("s") + ", $" + this.get("t");
    }
}
